﻿using System;
using System.Collections.Generic;

namespace EnumerableExtension.Test.CustomTypes
{
    public class IntegerByAbsComparer : IComparer<int>
    {
        public int Compare(int x, int y)
        {
            return Math.Abs(x).CompareTo(Math.Abs(y));
        }
    }
}