﻿using System;

namespace EnumerableExtension.Test.CustomTypes
{
    public class Employee : IEquatable<Employee>
    {
        public string Name { get; }
        public int Age { get; }
        public double Salary { get; }

        public static Employee[] GetEmployeeList()
        {
            var emp1 = new Employee("John Doe", 34, 350);
            var emp2 = new Employee("Jack Spade", 20, 477.4);
            var emp3 = new Employee("Howard Black", 19, 34);
            var emp4 = new Employee("Edward Doe", 55, 234);
            Employee[] employees = {emp1, emp2, emp3, emp4 };

            return employees;
        }
        
        public Employee(string name, int age, double salary)
        {
            Name = name;
            Age = age;
            Salary = salary;
        }

        public bool Equals(Employee other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Name == other.Name && Age == other.Age && Salary.Equals(other.Salary);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == this.GetType() && Equals((Employee) obj);
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash += 31 * this.Age.GetHashCode();
            hash += 31 * this.Name.GetHashCode();
            hash += 31 * this.Salary.GetHashCode();

            return hash;
        }
    }
}