﻿using System.Collections.Generic;

namespace EnumerableExtension.Test.CustomTypes
{
    /// <summary>
    /// Represents class for comparing strings by length.
    /// </summary>
    public class StringByLengthComparer : IComparer<string>
    {
        /// <summary>
        /// Compares strings by their length.
        /// </summary>
        /// <param name="x">First string.</param>
        /// <param name="y">Second string.</param>
        /// <returns>Result of comparison.</returns>
        public int Compare(string x, string y)
        {
            int xLength = string.IsNullOrEmpty(x) ? 0 : x.Length;
            int yLength = string.IsNullOrEmpty(y) ? 0 : y.Length;

            return xLength.CompareTo(yLength);
        }
    }
}