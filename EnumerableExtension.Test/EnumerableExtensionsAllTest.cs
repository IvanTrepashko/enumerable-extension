﻿using EnumerableExtension.Test.CustomTypes;
using NUnit.Framework;

namespace EnumerableExtension.Test
{
    [TestFixture]
    public class EnumerableExtensionsAllTest
    {
        [Test]
        public void All_Integer_MoreThanZero()
        {
            int[] source = {1, 2, 3, 4, 5, 7, 8};
            
            Assert.IsTrue(source.All(x => x > 0));
        }

        [Test]
        public void All_Employee_AgeMoreThan18()
        {
            var employees = Employee.GetEmployeeList();
            
            Assert.IsTrue(employees.All(x => x.Age > 18));
        }
    }
}