﻿using EnumerableExtension.Test.CustomTypes;
using NUnit.Framework;

namespace EnumerableExtension.Test
{
    [TestFixture]
    public class EnumerableExtensionsWhereTest
    {
        [Test]
        public void EnumerableWhereTest()
        {
            var source = Employee.GetEmployeeList();
            
            Employee[] expected =
            {
                new("John Doe", 34, 350),
                new("Edward Doe", 55, 234),
            };

            Assert.AreEqual(expected, source.Where(x => x.Age > 20));
        }
    }
}