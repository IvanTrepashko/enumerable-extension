﻿using NUnit.Framework;

namespace EnumerableExtension.Test
{
    [TestFixture]
    public class EnumerableExtensionsSelectTest
    {
        [Test]
        public void Select_Integer_ReturnsSquare()
        {
            int[] source = {1, 2, 3, 4, 5, 6};
            int[] expected = {1, 4, 9, 16, 25, 36};

            Assert.AreEqual(expected, source.Select(x => x * x));
        }

        [Test]
        public void Select_String_ReturnsToUpper()
        {
            string[] source = {"aaa", "bbb", "ccc", "ddd" };

            string[] expected = {"AAA", "BBB", "CCC", "DDD" };
            
            Assert.AreEqual(expected, source.Select(x => x.ToUpper()));
        }
    }
}