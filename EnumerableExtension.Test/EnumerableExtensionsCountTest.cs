﻿using NUnit.Framework;

namespace EnumerableExtension.Test
{
    [TestFixture]
    public class EnumerableExtensionsCountTest
    {
        [Test]
        public void TotalCountTest()
        {
            int[] source = {1, 2, 3, 4, 5, 6, 7, 8, 9};
            
            Assert.AreEqual(source.Length, source.Count());
        }

        [Test]
        public void Count_MatchesPredicate()
        {
            int[] source = {-1, 2, -3, 4, -5, 6, -7, 8};
            
            Assert.AreEqual(4, source.Count(x => x > 0));
        }
    }
}