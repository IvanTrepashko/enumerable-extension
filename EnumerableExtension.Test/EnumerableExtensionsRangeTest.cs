﻿using NUnit.Framework;

namespace EnumerableExtension.Test
{
    [TestFixture]
    public class EnumerableExtensionsRangeTest
    {
        [Test]
        public void RangeTest()
        {
            int[] expected = {-5, -4, -3, -2, -1, 0, 1, 2, 3};
            Assert.AreEqual(expected, EnumerableExtensions.Range(9,-5));
        }
    }
}