﻿using System;
using System.Collections.Generic;

namespace EnumerableExtension
{
    /// <summary>
    /// Implements array helper methods.
    /// </summary>
    public static class BufferData
    {
        /// <summary>
        /// Creates array on base of enumerable sequence.
        /// </summary>
        /// <param name="source">The enumerable sequence.</param>
        /// <typeparam name="T">Type of the elements of the sequence.</typeparam>
        /// <returns>Single dimension zero based array.</returns>
        public static T[] ToArray<T>(IEnumerable<T> source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }
            
            int currentIndex = 0;
            T[] array = Array.Empty<T>();
            
            if (source is ICollection<T> collection)
            {
                Array.Resize(ref array, collection.Count);                
                foreach (var item in source)
                {
                    array[currentIndex] = item;
                    currentIndex++;
                }

                return array;
            }

            foreach (var item in source)
            {
                if (currentIndex == array.Length)
                {
                    Array.Resize(ref array, array.Length == 0 ? 1 : array.Length * 2);
                }

                array[currentIndex] = item;
                currentIndex++;
            }

            return array;
        }
    }
}