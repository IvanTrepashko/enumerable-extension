﻿using System;
using System.Collections.Generic;

namespace EnumerableExtension
{
    /// <summary>
    /// Represents extension methods for IEnumerable.
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Filters a sequence of values based on a predicate. 
        /// </summary>
        /// <param name="source">Source sequence.</param>
        /// <param name="predicate">Predicate to filter by.</param>
        /// <typeparam name="TSource">Type of source sequence.</typeparam>
        /// <returns>Filtered sequence.</returns>
        /// <exception cref="ArgumentNullException">Thrown when predicate is null.</exception>
        public static IEnumerable<TSource> Where<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            if (predicate is null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }
            
            foreach (var item in source)
            {
                if (predicate(item))
                {
                    yield return item;
                }
            }
        }

        /// <summary>
        /// Transforms sequence according to specified rule.
        /// </summary>
        /// <param name="source">Source sequence.</param>
        /// <param name="transform">Transform rule.</param>
        /// <typeparam name="TSource">Type of source sequence.</typeparam>
        /// <typeparam name="TResult">Type of result sequence.</typeparam>
        /// <returns>Transformed sequence.</returns>
        /// <exception cref="ArgumentNullException">Thrown when transform rule is null.</exception>
        public static IEnumerable<TResult> Select<TSource, TResult>(this IEnumerable<TSource> source,
            Func<TSource, TResult> transform)
        {
            if (transform is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            foreach (var item in source)
            {
                yield return transform(item);
            }
        }

        /// <summary>
        /// Transform sequence into array.
        /// </summary>
        /// <param name="source">Source sequence.</param>
        /// <typeparam name="T">Type of source sequence.</typeparam>
        /// <returns>Array based on source sequence.</returns>
        public static T[] ToArray<T>(this IEnumerable<T> source)
        {
            if (source is ICollection<T> collection)
            {
                T[] array = new T[collection.Count];
                int index = 0;

                foreach (var item in source)
                {
                    array[index] = item;
                    index++;
                }

                return array;
            }
            else
            {
                T[] array = new T[4];
                int index = 0;

                foreach (var item in source)
                {
                    if (index == array.Length)
                    {
                        Array.Resize(ref array, array.Length * 2);
                    }
                    
                    array[index] = item;
                    index++;
                }

                return array[..index];
            }
        }

        /// <summary>
        /// Orders sequence by specified key.
        /// </summary>
        /// <param name="source">Source sequence.</param>
        /// <param name="func">Key.</param>
        /// <typeparam name="TSource">Type of sequence.</typeparam>
        /// <typeparam name="TKey">Type of key.</typeparam>
        /// <returns>Ordered sequence.</returns>
        /// <exception cref="ArgumentNullException">Thrown when func is null.</exception>
        public static IEnumerable<TSource> OrderBy<TSource, TKey>(this IEnumerable<TSource> source,
            Func<TSource, TKey> func)
        {
            if (func is null)
            {
                throw new ArgumentNullException(nameof(func));
            }

            var comparer = Comparer<TKey>.Default;

            var result = BufferData.ToArray(source);

            for (int i = 1; i < result.Length; i++)
            {
                TSource temp = result[i];
                for (int j = i - 1; j >= 0; j--)
                {
                    if (comparer.Compare(func(result[j]), func(temp)) < 0)
                    {
                        break;
                    }

                    if (comparer.Compare(func(result[j + 1]), func(result[j])) < 0)
                    {
                        Swap(ref result[j], ref result[j + 1]);
                    }
                }
            }

            return result;
        }
        
        /// <summary>
        /// Orders sequence by specified key using custom comparer.
        /// </summary>
        /// <param name="source">Source sequence.</param>
        /// <param name="func">Key.</param>
        /// <param name="comparer">Comparer.</param>
        /// <typeparam name="TSource">Type of sequence.</typeparam>
        /// <typeparam name="TKey">Type of key.</typeparam>
        /// <returns>Ordered sequence.</returns>
        /// <exception cref="ArgumentNullException">Thrown when func is null.</exception>
        public static IEnumerable<TSource> OrderBy<TSource, TKey>(this IEnumerable<TSource> source,
            Func<TSource, TKey> func, IComparer<TKey> comparer)
        {
            if (func is null)
            {
                throw new ArgumentNullException(nameof(func));
            }

            comparer ??= Comparer<TKey>.Default;

            var result = BufferData.ToArray(source);

            for (int i = 1; i < result.Length; i++)
            {
                TSource temp = result[i];
                for (int j = i - 1; j >= 0; j--)
                {
                    if (comparer.Compare(func(result[j]), func(temp)) < 0)
                    {
                        break;
                    }

                    if (comparer.Compare(func(result[j + 1]), func(result[j])) < 0)
                    {
                        Swap(ref result[j], ref result[j + 1]);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Casts sequence of objects into specified type.
        /// </summary>
        /// <param name="source">Source sequence.</param>
        /// <typeparam name="TResult">Type of sequence.</typeparam>
        /// <returns>Result sequence.</returns>
        public static IEnumerable<TResult> Cast<TResult>(this IEnumerable<object> source)
        {
            foreach (var item in source)
            {
                yield return (TResult)item;
            }
        }
        
        /// <summary>
        /// Filters the elements of source sequence based on a specified type.
        /// </summary>
        /// <typeparam name="TResult">Type selector to return.</typeparam>
        /// <param name="source">The source sequence.</param>
        /// <returns>A sequence that contains the elements from source sequence that have type TResult.</returns>
        /// <exception cref="ArgumentNullException">Thrown when sequence is null.</exception>
        public static IEnumerable<TResult> TypeOf<TResult>(this IEnumerable<object> source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            foreach (var item in source)
            {
                if (item?.GetType() == typeof(TResult))
                {
                    yield return (TResult)item;
                }
            }
        }

        /// <summary>
        /// Determines if all elements of the sequence match specified predicate.
        /// </summary>
        /// <param name="source">Source sequence.</param>
        /// <param name="predicate">Predicate.</param>
        /// <typeparam name="TSource">Type of source sequence.</typeparam>
        /// <returns>True if all elements match predicate, else returns false.</returns>
        /// <exception cref="ArgumentNullException">Thrown when predicate is null.</exception>
        public static bool All<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            if (predicate is null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }
            
            foreach (var item in source)
            {
                if (!predicate(item))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Orders sequence in descending order by specified key.
        /// </summary>
        /// <param name="source">Source sequence.</param>
        /// <param name="func">Key.</param>
        /// <typeparam name="TSource">Type of sequence.</typeparam>
        /// <typeparam name="TKey">Type of key.</typeparam>
        /// <returns>Ordered sequence.</returns>
        /// <exception cref="ArgumentNullException">Thrown when func is null.</exception>
        public static IEnumerable<TSource> OrderByDescending<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> func)
        {
            if (func is null)
            {
                throw new ArgumentNullException(nameof(func));
            }

            var comparer = Comparer<TKey>.Default;

            var result = BufferData.ToArray(source);

            for (int i = 1; i < result.Length; i++)
            {
                TSource temp = result[i];
                for (int j = i - 1; j >= 0; j--)
                {
                    if (comparer.Compare(func(result[j]), func(temp)) > 0)
                    {
                        break;
                    }

                    if (comparer.Compare(func(result[j + 1]), func(result[j])) > 0)
                    {
                        Swap(ref result[j], ref result[j + 1]);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Orders sequence in descending order by specified key using custom comparer.
        /// </summary>
        /// <param name="source">Source sequence.</param>
        /// <param name="func">Key.</param>
        /// <param name="comparer">Comparer.</param>
        /// <typeparam name="TSource">Type of sequence.</typeparam>
        /// <typeparam name="TKey">Type of key.</typeparam>
        /// <returns>Ordered sequence.</returns>
        /// <exception cref="ArgumentNullException">Thrown when func is null.</exception>
        public static IEnumerable<TSource> OrderByDescending<TSource, TKey>(this IEnumerable<TSource> source,
            Func<TSource, TKey> func, IComparer<TKey> comparer)
        {
            if (func is null)
            {
                throw new ArgumentNullException(nameof(func));
            }

            comparer ??= Comparer<TKey>.Default;

            var result = BufferData.ToArray(source);

            for (int i = 1; i < result.Length; i++)
            {
                TSource temp = result[i];
                for (int j = i - 1; j >= 0; j--)
                {
                    if (comparer.Compare(func(result[j]), func(temp)) > 0)
                    {
                        break;
                    }

                    if (comparer.Compare(func(result[j + 1]), func(result[j])) >= 0)
                    {
                        Swap(ref result[j], ref result[j + 1]);
                    }
                }
            }

            return result;
        }
        
        /// <summary>
        /// Inverts the order of the elements in a sequence.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of sequence.</typeparam>
        /// <param name="source">A sequence of elements to reverse.</param>
        /// <exception cref="ArgumentNullException">Thrown when sequence is null.</exception>
        /// <returns>Reversed IEnumerable sequence.</returns>
        public static IEnumerable<TSource> Reverse<TSource>(this IEnumerable<TSource> source)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            var result = BufferData.ToArray(source);
            
            for (int i = 0; i < result.Length / 2; i++)
            {
                TSource tmp = result[i];
                result[i] = result[result.Length - i - 1];
                result[result.Length - i - 1] = tmp;
            }

            return result;
        }

        /// <summary>
        /// Returns range of numbers starting from start.
        /// </summary>
        /// <param name="count">Count of elements.</param>
        /// <param name="start">Starting number.</param>
        /// <returns>Generated sequence.</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when count is less or equal to 0.</exception>
        public static IEnumerable<int> Range(int count, int start)
        {
            if (count <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(count));
            }

            for (int i = 0; i < count; i++)
            {
                yield return start + i;
            }
        }

        /// <summary>
        /// Returns count of elements that match specified predicate.
        /// </summary>
        /// <param name="source">Source sequence.</param>
        /// <param name="predicate">Predicate.</param>
        /// <typeparam name="TSource">Type of source sequence.</typeparam>
        /// <returns>Count of elements.</returns>
        public static int Count<TSource>(this IEnumerable<TSource> source,
            Func<TSource, bool> predicate)
        {
            int count = 0;

            foreach (var item in source)
            {
                if (predicate(item))
                {
                    count++;
                }
            }

            return count;
        }

        /// <summary>
        /// Returns number of elements in a sequence.
        /// </summary>
        /// <param name="source">Source sequence.</param>
        /// <typeparam name="TSource">Type of source sequence.</typeparam>
        /// <returns>Count of elements.</returns>
        public static int Count<TSource>(this IEnumerable<TSource> source)
        {
            if (source is ICollection<TSource> collection)
            {
                return collection.Count;
            }

            int count = 0;

            foreach (var unused in source)
            {
                count++;
            }

            return count;
        }
        
        /// <summary>
        /// Swaps two objects.
        /// </summary>
        /// <typeparam name="T">The type of parameters.</typeparam>
        /// <param name="left">First object.</param>
        /// <param name="right">Second object.</param>
        private static void Swap<T>(ref T left, ref T right)
        {
            T tmp = left;
            left = right;
            right = tmp;
        }
    }
}